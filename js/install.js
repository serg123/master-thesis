
class AppInstaller{
    constructor(appExporter){
        this.apiURL='https://pwa-list.000webhostapp.com/api/install.php';
        this.bsGridURL='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap-grid.min.css';
        this.vueURL='https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.13/vue.min.js';
        this.appExporter=appExporter;
    }
    findInclude(node,include){
        for(let n of node.values()){
            if(n instanceof Col){
                let lines=n.component.include.split('\n');
                lines = app.vm.stripComments(lines);
                include.push(...lines);
            }
            else
                this.findInclude(n,include);
        }
    }
    install(app){
        let include=app.vm.stripComments(app.vm.include.split('\n'));
        this.findInclude(app,include);
        fetch(this.apiURL,{
            method:'post',
            body:JSON.stringify({
                name:app.name,
                app:this.appExporter.export('sw.js'),
                manifest:{name:app.name,short_name:app.name,...manifest},
                cache:[
                    'index.html',
                    this.vueURL,
                    this.bsGridURL
                ].concat(include)
            })
        }).then(()=>{
            window.open(this.apiURL+'/../'+encodeURI(app.name)+'/index.html?'+Math.random(), '_blank');
        });
    }
}