class App extends Map{
    constructor(component, name='app'){
        super();
        this.name=name;
        this.vm=component;
    }
    findParent(name){
        return this._findParent(name,this);
    }
    _findParent(name,node){
        if(node instanceof Map){
            for(let k of node.keys()) {
                if (name == k)
                    return node;
                else {
                    parent = this._findParent(name, node.get(k));
                    if (parent)
                        return parent;
                }
            }
        }
    }
    serialize(){
        return {
            name: this.name,
            vm: this.vm.serialize(),
            children:[...this.values()].map(e=>e.serialize()),
        }
    }
    unserialize(image){
        this.clear();
        this.name=image.name;
        this.vm.unserialize(image.vm);
        for(let c of image.children)
            this.set(c.name, new Layer(c.name).unserialize(c))
    }
}

class Layer extends Map{
    constructor(name){
        super();
        this.name=name;
        this.desktop={position:'absolute'};
        this.mobile={position:'absolute'};
        this.properties=new Map();
        this.propTypes=new Map();
        this.properties.set('display',null);
        this.propTypes.set('display','Boolean');
    }
    serialize(){
        return{
            type: 'layer',
            name: this.name,
            desktop: this.desktop,
            mobile: this.mobile,
            properties: [...this.properties],
            propTypes: [...this.propTypes],
            children:[...this.values()].map(e=>e.serialize())
        }
    }
    unserialize(image){
        this.clear();
        this.properties.clear();
        this.propTypes.clear();
        this.desktop=image.desktop;
        this.mobile=image.mobile;
        image.properties.forEach(kv=>this.properties.set(kv[0],kv[1]));
        image.propTypes.forEach(kv=>this.propTypes.set(kv[0],kv[1]));
        for(let c of image.children)
            this.set(c.name, new Row(c.name).unserialize(c));
        return this;
    }
}

class Row extends Map{
    constructor(name){
        super();
        this.name=name;
        this.desktop={width:6,offset:0,align:'start',pl:0,pt:0,pr:0,pb:0};
        this.mobile={width:6,offset:0,align:'start', pl:0,pt:0,pr:0,pb:0};
        this.properties=new Map();
        this.propTypes=new Map();
        this.properties.set('display',null);
        this.propTypes.set('display','Boolean');
    }
    serialize(){
        return{
            type: 'row',
            name: this.name,
            desktop: this.desktop,
            mobile: this.mobile,
            properties: [...this.properties],
            propTypes: [...this.propTypes],
            children:[...this.values()].map(e=>e.serialize())
        }
    }
    unserialize(image){
        this.clear();
        this.properties.clear();
        this.propTypes.clear();
        this.desktop=image.desktop;
        this.mobile=image.mobile;
        image.properties.forEach(kv=>this.properties.set(kv[0],kv[1]));
        image.propTypes.forEach(kv=>this.propTypes.set(kv[0],kv[1]));
        for(let c of image.children){
            if(c.type=='row')
                this.set(c.name, new Row(c.name).unserialize(c))
            else
                this.set(c.name, new Col(c.name,new Component().unserialize(c.component)).unserialize(c))
        }
        return this;
    }
}

class Col{
    constructor(name,component){
        this.name=name;
        this.desktop={width:6,offset:0,align:'start',pl:0,pt:0,pr:0,pb:0};
        this.mobile={width:6,offset:0,align:'start', pl:0,pt:0,pr:0,pb:0};
        this.component=component;
        this.events=new Map();
        for(let e of component.events)
            this.events.set(e,'function(){\n\t\n}');
        this.extractSlots();
        this.style=component.style;
        let str='this.props={'+component.props.replace(/\s/g,'')+'}';
        eval(str);
        this.properties=new Map();
        this.propTypes=new Map();
        for (let p in this.props) {
            if (this.props.hasOwnProperty(p)){
                this.properties.set(p,null);
                let type='';
                if(this.props[p].type)
                    type=this.props[p].type.name;
                this.propTypes.set(p,type);
            }
        }
        let keys=[...this.properties.keys()];
        this.bind2way(keys,component.hooks);
        this.bind2way(keys,component.methods);
        this.bind2way(keys,component.computed);
        this.bind2way(keys,component.watchers);
        this.properties.set('display',null);
        this.propTypes.set('display','Boolean');
    }
    extractSlots(){
        this.slots=new Map();
        this.slotArgs=new Map();
        let matches=this.component.template.match(/<slot.*name=['"]\w+['"](.|\n)*?<\/slot>/ig);
        if(matches){
            for(let m of matches){
                let matches=m.match(/name=['"]\w+['"]/i);
                let key=matches[0].substring(6, matches[0].length - 1);
                matches=m.match(/>(.|\n)*<\/slot>/ig);
                let value=matches[0].substring(1, matches[0].length - 7).trim();
                this.slots.set(key,value);
                matches=m.match(/<slot.*>/);
                matches=matches[0].match(/:[a-z0-9_$]+/ig);
                this.slotArgs.set(key,[]);
                if(matches){
                    for(let m of matches)
                        this.slotArgs.get(key).push(m.substring(m.indexOf(':')+1));
                }
            }
        }
    }
    bind2way(properties,functions){
        if(properties.length>0 && functions.size>0) {
            let propRegEx = new RegExp('this\\.(' + properties.join('|') + ')\\s*=\\s*(.+?)(;|\\n)', 'g');
            for (let [k, v] of functions) {
                v=v.replace(propRegEx,'this.$emit(\'update:$1\',$2);');
                functions.set(k,v);
            }
        }
    }
    serialize(){
        return{
            type: 'col',
            name: this.name,
            desktop: this.desktop,
            mobile: this.mobile,
            properties: [...this.properties],
            propTypes: [...this.propTypes],
            slots: [...this.slots],
            events: [...this.events],
            style: this.style,
            component: this.component.serialize()
        }
    }
    unserialize(image){
        this.properties.clear();
        this.propTypes.clear();
        this.slots.clear();
        this.events.clear();
        this.name=image.name;
        this.desktop=image.desktop;
        this.mobile=image.mobile;
        image.properties.forEach(kv=>this.properties.set(kv[0],kv[1]));
        image.propTypes.forEach(kv=>this.propTypes.set(kv[0],kv[1]));
        image.slots.forEach(kv=>this.slots.set(kv[0],kv[1]));
        image.events.forEach(kv=>this.events.set(kv[0],kv[1]));
        this.style=image.style;
        return this;
    }
}