class LocalComponentLibrary{
    constructor(){
        this.databaseName='LocalComponentLibrary';
        this.databaseVersion=1;
        this.storeName='ComponentStore';
    }
    get(name) {
        return new Promise(resolve=>{
            let open = indexedDB.open(this.databaseName, this.databaseVersion);
            open.onupgradeneeded = this.defineSchema;
            open.onsuccess = function () {
                let db = open.result;
                let tx = db.transaction(this.storeName, "readonly");
                let store = tx.objectStore(this.storeName);
                let request = store.get(name);
                request.onsuccess = function () {
                    let c=request.result.component;
                    if(c.parent)
                        c.name=c.name.substring(0,c.name.indexOf(c.parent)-1);
                    c.name=c.name.toLowerCase();
                    resolve(c);
                }
                tx.oncomplete = function () {
                    db.close();
                };
            }
        });
    }
    set(component){
        if(component.parent)
            component.name+=('('+component.parent+')');
        component.name=component.name.toLowerCase();
        let open = indexedDB.open(this.databaseName, this.databaseVersion);
        open.onupgradeneeded = this.defineSchema;
        open.onsuccess=function(){
            let db = open.result;
            let tx = db.transaction(this.storeName, "readwrite");
            let store = tx.objectStore(this.storeName);
            store.put({name:component.name, component: component});
            tx.oncomplete = function() {
                db.close();
            };
        }
    }
    delete(name) {
        return new Promise(resolve=>{
            let open = indexedDB.open(this.databaseName, this.databaseVersion);
            open.onupgradeneeded = this.defineSchema;
            open.onsuccess = function () {
                let db = open.result;
                let tx = db.transaction(this.storeName, "readwrite");
                let store = tx.objectStore(this.storeName);
                let request = store.delete(name);
                request.onsuccess = function () {
                    resolve();
                }
                tx.oncomplete = function () {
                    db.close();
                };
            }
        });
    }
    all() {
        return new Promise(resolve=>{
            let open = indexedDB.open(this.databaseName, this.databaseVersion);
            open.onupgradeneeded = this.defineSchema;
            open.onsuccess = function () {
                let db = open.result;
                let tx = db.transaction(this.storeName, "readonly");
                let store = tx.objectStore(this.storeName);
                let request = store.getAllKeys();
                request.onsuccess = function () {
                    resolve(request.result);
                }
                tx.oncomplete = function () {
                    db.close();
                };
            }
        });
    }
    defineSchema(event){
        let db = event.target.result;
        self.store = db.createObjectStore(this.storeName, { keyPath: "name" });
    }
}


class RemoteComponentLibrary {
    constructor() {
        this.apiURL='http://193.40.11.226/api/library.php';
    }
    set(component){
        if(component.parent)
            component.name+=('('+component.parent+')');
        component.name=component.name.toLowerCase();
        fetch(this.apiURL,{
            method:'post',
            body:JSON.stringify({
                method:'set',
                name:component.name,
                component:component
            })
        });
    }
    all() {
        return new Promise(resolve=>{
            fetch(this.apiURL,{
                method:'post',
                body:JSON.stringify({method:'all',})
            }).then(function(response){
                response.json().then(function(names){
                    resolve(names);
                })
            })
        });
    }
    delete(name) {
        return new Promise(resolve=>{
            fetch(this.apiURL,{
                method:'post',
                body:JSON.stringify({
                    method:'delete',
                    name:name
                })
            }).then(function(response){
                resolve();
            })
        });
    }
    get(name) {
        return new Promise(resolve=>{
            fetch(this.apiURL,{
                method:'post',
                body:JSON.stringify({
                    method:'get',
                    name:name
                })
            }).then(function(response){
                response.json().then(function(c){
                    if(c.parent)
                        c.name=c.name.substring(0,c.name.indexOf(c.parent)-1);
                    c.name=c.name.toLowerCase();
                    resolve(c);
                })
            })
        });
    }
}