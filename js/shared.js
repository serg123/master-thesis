function initCodeMirror(selector,mode, readonly=false){
    let textArea=$(selector)[0];
    let rate=readonly?-500:500;
    let settings = {
        mode: mode,
        lineNumbers: true,
        indentUnit: 4,
        readOnly:readonly,
        lineWrapping: false,
        cursorBlinkRate: rate,
        indentWithTabs:true
    };
    return CodeMirror.fromTextArea(textArea,settings);
}

class PreviewController{
    constructor(previewSelector,previewExporter,previewCode,vueExporter){
        this.previewExporter=previewExporter;
        this.preview=$(previewSelector);
        this.vueExporter=vueExporter;
        this.previewCode=previewCode;
        let self=this;
        window.onmessage=function(e) {
            let {version,at,err,line}=e.data;
            let lines = self.lastPreviewString.split('\n');
            let content = 'data:text/html;charset=utf-8,<body style="margin:0px; background:#eeeeee; color:#660000; padding:5%;"><div>Version: ' + version +
                '<br>At: ' + at + ' (line: ' + line + ')' +
                '<br>Error: ' + err +
                '<br><pre style="font-size:1.1em; color:black">' + lines[line - 2] + '\n' +
                '<span style="color:#ff0000">' + lines[line - 1] + '\n</span>' +
                lines[line] + '</pre>' +
                '</div></body>';
            if(at.search('render')!=-1){
                content = 'data:text/html;charset=utf-8,<body style="margin:0px; background:#eeeeee; color:#660000; padding:5%;"><div>Version: ' + version +
                    '<br>At: template' +
                    '<br>Error: ' + err +
                    '</div></body>';
            }
            self.preview.prop('src',content)
        }
        this.isPreview=true;
        $('a[href="#codeTab"]').on('shown.bs.tab',function(){
            self.isPreview=false;
            self.update();
        });
        $('a[href="#previewTab"]').on('shown.bs.tab',function(){
            self.isPreview=true;
            self.update();
        });
    }
    update(){
        if(this.isPreview){
            this.lastPreviewString=this.previewExporter.export().replace(/#/g,'%23');
            this.preview.prop('src',this.lastPreviewString);
        }
        else{
            this.previewCode.getDoc().setValue(this.vueExporter.export());
            this.previewCode.refresh();
            this.previewCode.focus();
        }
    }
}

//allows using one name-input modal
function openName(selector, callback, nameOnly=true){
    let modal=$(selector);
    let field=$(selector+' input[type="text"]');
    modal.modal('show');
    field.focus();
    field.off("keypress");
    field.on("keypress", function (e) {
        if (e.which == 13){
            modal.modal('hide');
            let name=field.prop('value');
            callback(name);
        }
    });
    if(nameOnly){
        field.prop('value', '');
    }
}



// hooks
function newHook(name){
    $('#hooks').append(`<a href="javascript:void(0)" class="list-group-item list-group-item-action text-primary" onclick="openCode('hooks','${name}')">${name}</a>`);
    component.hooks.init(name);
}

//methods
function newMethod(name){
    component.methods.init(name);
    updateMethods();
    openCode('methods', name);
}
function deleteMethod(name){
    component.methods.delete(name);
    updateMethods();
	updateWatchers();
	updateList('#events',component.events);
}
function updateMethods(){
    let parent=$('#methods');
    parent.empty();
    for (name of component.methods.keys()) {
        parent.append(`<a href="javascript:void(0)" class="list-group-item list-group-item-action text-primary" onclick="openCode('methods','${name}')">${name}
        <button type="button" class="btn btn-link" onclick="deleteMethod('${name}'); event.cancelBubble=true"><i class="fa fa-minus text-danger"></i></button></a>`);
    }
}

//computed
function newComputed(name){
    component.computed.init(name);
    updateComputed();
    openCode('computed', name);
}
function deleteComputed(name){
    component.computed.delete(name);
    updateComputed();
	updateWatchers();
	updateList('#events',component.events);
}
function updateComputed(){
    let parent=$('#computed');
    parent.empty();
    for (name of component.computed.keys()) {
        parent.append(`<a href="javascript:void(0)" class="list-group-item list-group-item-action text-primary" onclick="openCode('computed','${name}')">${name}
        <button type="button" class="btn btn-link" onclick="deleteComputed('${name}'); event.cancelBubble=true"><i class="fa fa-minus text-danger"></i></button></a>`);
    }
}

//watchers
function updateWatchers(){
    let parent=$('#watchers');
    parent.empty();
    for (let name of component.data) {
        parent.append(`<a href="javascript:void(0)" class="list-group-item list-group-item-action text-primary" onclick="openCode('watchers','${name}')">${name}</a>`);
        if(!component.watchers.has(name))
            component.watchers.init(name, 'newValue', 'oldValue');
    }
    for (let key of component.watchers.keys()) {
        if(!component.data.has(key))
            component.watchers.delete(key);
    }
}

//open, close+save for all code-based modals
function openCode(type, name) {
    let text = component[type].get(name);
    let modal=$('#codeModal');
    modal.modal('show');
    editors.code.focus()
    editors.code.getDoc().setValue(text);
    editors.code.setCursor({line:1, ch:1});
    modal.off('hidden.bs.modal');
    modal.on('hidden.bs.modal', function(){closeCode(type,name)});
    editors.code.refresh();
}
function closeCode(type, name) {
    text=editors.code.getDoc().getValue();
    component[type].set(name, text);
    updateList('#events',component.events);
    updateWatchers();
    if(type='methods') updateMethods();
    if(type='computed') updateComputed();
    if(typeof previewController!='undefined') previewController.update();   //for component
    else openSelection();                                                   //for builder
}

// for style,properties and template
function openEditor(modalSelector, type) {
    let text = component[type];
    let modal=$(modalSelector);
    modal.modal('show');
    editors[type].focus()
    editors[type].getDoc().setValue(text);
    editors[type].setCursor(1,0);
    editors[type].refresh();
}
function closeEditor(type) {
    let text=editors[type].getDoc().getValue();
    component[type]=text;
    if(type=='template')
        updateList('#slots',component.slots);
    if(typeof previewController!='undefined') previewController.update();
}

//for display-only lists like watchers and slots
function updateList(parentSelector,items){
    let parent=$(parentSelector);
    parent.empty();
    for(let item of items)
        parent.append(`<a href="javascript:void(0)" class="list-group-item passive-list-group-item">${item}</a>`);
}