class RemoteAppLibrary {
    constructor() {
        this.apiURL='http://193.40.11.226/api/app.php';
    }
    set(app){
        fetch(this.apiURL,{
            method:'post',
            body:JSON.stringify({
                method:'set',
                name:app.name,
                component:app
            })
        });
    }
    all() {
        return new Promise(resolve=>{
            fetch(this.apiURL,{
                method:'post',
                body:JSON.stringify({method:'all',})
            }).then(function(response){
                response.json().then(function(names){
                    resolve(names);
                })
            })
        });
    }
    delete(name) {
        return new Promise(resolve=>{
            fetch(this.apiURL,{
                method:'post',
                body:JSON.stringify({
                    method:'delete',
                    name:name
                })
            }).then(function(response){
                resolve();
            })
        });
    }
    get(name) {
        return new Promise(resolve=>{
            fetch(this.apiURL,{
                method:'post',
                body:JSON.stringify({
                    method:'get',
                    name:name
                })
            }).then(function(response){
                response.json().then(function(app){
                    resolve(app);
                })
            })
        });
    }
}