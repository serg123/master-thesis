class Component{
    constructor(){
        this.reset();
    }
    get template(){return this._template}
    set template(v){
        this.slots.clear();
        let matches=v.match(/<slot.*name=['"]\w+['"]/ig);
        if(matches){
            for(let m of matches){
                let matches=m.match(/name=['"]\w+['"]/i);
                this.slots.add(matches[0].substring(6, matches[0].length - 1));
            }
        }
        this._template=v;
    }
    get properties(){return this._properties}
    set properties(v) {
        let types=['string','number','boolean','function','object','array','symbol'];
        this._properties=v;
        let lines=v.split('\n');
        lines = this.stripComments(lines);
        let result=[];
        for(let l of lines) {
            let tokens = l.match(/\S+/g) || [];
            if(tokens.length==1){
                result.push(tokens[0]+':{\n}');
            }
            else if(tokens.length==2 && types.includes(tokens[0].toLowerCase())){
                result.push(tokens[1]+':{\n\ttype:'+this.makeType(tokens[0])+'\n}');
            }
            else if(tokens.length==2){
                result.push(tokens[0]+':{\n\tdefault:'+tokens[1]+'\n}');
            }
            else if(tokens.length==3){
                result.push(tokens[1]+':{\n\ttype:'+this.makeType(tokens[0])+',\n\tdefault:'+tokens[2]+'\n}');
            }
        }
        this.props=result.join(',\n');
    }
    get include(){return this._include}
    set include(v){
        this._include=v;
        let lines=v.split('\n');
        lines = this.stripComments(lines);
        let result=[];
        for(let l of lines){
            if(l.match(/\.css$/i)){
                result.push('<link rel="stylesheet" href="'+l+'">');
            }
            else if(l.match(/\.js$/i)){
                result.push('<script src="'+l+'"></script>');
            }
        }
        this.head=result.join('\n');
    }
    get events(){
        return this.unionAll('events');
    }
    get data(){
        let result=this.unionAll('data');
        result.toString=function(){return [...this.values()].map(e=>e+':undefined').join(',\n')}
        return result;
    }
    unionAll(type){
        let result=new Set();
        for(let e of this.hooks[type].values())
            result=this.union(result,e);
        for(let e of this.methods[type].values())
            result=this.union(result,e);
        for(let e of this.computed[type].values())
            result=this.union(result,e);
        for(let e of this.watchers[type].values())
            result=this.union(result,e);
        return result;
    }
    union(set1,set2){
        for (let e of set2)
            set1.add(e);
        return set1;
    }
    makeType(type){
        type = type.toLowerCase();
        return type.charAt(0).toUpperCase() + type.substring(1);
    }
    stripComments(lines){
        lines=lines.map((e)=>{
            let i=e.indexOf('#')
            return i==-1 ? e : e.substring(0,i);
        });
        return lines.reduce((r,e)=> {
            if(e) r.push(e);
            return(r);
        },[]);
    }
    serialize(){
        return {
            name: this.name,
            parent: this.parent,
            template: this._template,
            properties: this._properties,
            style: this.style,
            include: this.include,
            hooks: [...this.hooks.entries()],
            methods: [...this.methods.entries()],
            computed: [...this.computed.entries()],
            watchers: [...this.watchers.entries()]
        }
    }
    unserialize(image){
        this.reset();
        this.name=image.name;
        this.parent=image.parent;
        this.template=image.template;
        this.properties=image.properties;
        this.style=image.style;
        this.include=image.include;
        image.hooks.forEach(kv=>this.hooks.set(kv[0],kv[1]));
        image.methods.forEach(kv=>this.methods.set(kv[0],kv[1]));
        image.computed.forEach(kv=>this.computed.set(kv[0],kv[1]));
        image.watchers.forEach(kv=>this.watchers.set(kv[0],kv[1]));
        return this;
    }
    reset(){
        this.slots=new Set();
        this.hooks=new Functions();
        this.methods=new Functions(true);
        this.computed=new Functions(true);
        this.watchers=new Functions();
        this.head='';
        this.name='comp';
        this.parent=null;
        this.template='<!--directives: {{expr}}, v-html, :attr, @event, v-if, v-for, v-model-->\n';
        this.properties='# [type] name [value]\n';
        this.style='/*normal css here*/\n';
        this.include='# http://examplecdn.com/css/style.css\n';
    }
}

class Functions extends Map{
    constructor(changeable=false){
        super();
        this.events=new Map();
        this.data=new Map();
        this.changeable=changeable;
    }
    init(key,...args){
        super.set(key, key+'('+args.join(',')+'){\n\t\n}');
    }
    set(key,value){
        let index=value.indexOf('(');
        let newKey = value.substring(0, index).trim();
        let other = value.substring(index);
        if(key!=newKey && this.changeable){
            super.delete(key);
            this.events.delete(key);
            this.data.delete(key);
            key=newKey;
        }
        value=key+other;
        super.set(key,value);
        this.events.set(key,this.extractEvents(value));
        this.data.set(key,this.extractData(value));
    }
	delete(key){
        this.events.delete(key);
        this.data.delete(key);
        super.delete(key)
    }
    toString() {
        let entries=[...this.entries()];
        return entries.map(kv=>kv[1]).join(',\n');
    }
    extractEvents(text){
        let events=new Set();
        let matches=text.match(/this\.\$emit\(["'].+?["']/g);
        if(matches){
            for(let m of matches)
                events.add(m.substring(12,m.length-1));
        }
        return events;
    }
    extractData(text){
        let data=new Set();
        let matches=text.match(/this\.[a-zA-Z0-9_$]+[=\s;}]/g);
        if(matches){
            for(let m of matches)
                data.add(m.substring(5,m.length-1));
        }
        return data;
    }
}
