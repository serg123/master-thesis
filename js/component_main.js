onload=function(){
    editors= {
        template: initCodeMirror('#template', 'htmlmixed'),
        properties: initCodeMirror('#properties', 'properties'),
        style: initCodeMirror('#style', 'css'),
        code: initCodeMirror('#code', 'javascript'),
        include: initCodeMirror('#include', 'properties'),
        previewCode: initCodeMirror('#previewCode', 'htmlmixed', true)
    }
    component = new Component();
    vueExporter = new VueExporter(component);
    previewExporter=new PreviewExporter(component);
    previewController=new PreviewController('#preview',previewExporter,editors.previewCode,vueExporter);
    localComponentLibrary=new LocalComponentLibrary();
    remoteComponentLibrary=new RemoteComponentLibrary();
    newHook('created');
    newHook('mounted');
    newHook('updated');
    newHook('destroyed');
    $('#templateModal').on('hidden.bs.modal', function(){closeEditor('template')});
    $('#propertiesModal').on('hidden.bs.modal', function(){closeEditor('properties')});
    $('#styleModal').on('hidden.bs.modal', function(){closeEditor('style')});
    $('#includeModal').on('hidden.bs.modal', function(){closeEditor('include')});
    $('#configurationModal').on('hidden.bs.modal', function(){configure(false)});
	component.name='comp';
	$('#saveName').val(component.name);
    configure(false);
    previewController.update();
}




// configuration, reset and download
function configure(configure=true){
    if(configure){
        remoteComponentLibrary.apiURL=$('#configurationModal #apiURL').val();
        previewExporter.vueURL=$('#configurationModal #vueURL').val();
        previewExporter.bsGridURL=$('#configurationModal #bsGridURL').val();
        previewExporter.bgURL=$('#configurationModal #bgURL').val();
        previewExporter.border=$('#configurationModal #border').val();
        previewExporter.width=$('#configurationModal #width').val();
        previewController.update();
        $('#configurationModal').modal('hide');
    }
    else{
        $('#configurationModal #apiURL').val(remoteComponentLibrary.apiURL);
        $('#configurationModal #vueURL').val(previewExporter.vueURL);
        $('#configurationModal #bsGridURL').val(previewExporter.bsGridURL);
        $('#configurationModal #bgURL').val(previewExporter.bgURL);
        $('#configurationModal #border').val(previewExporter.border);
        $('#configurationModal #width').val(previewExporter.width);
    }
}

function reset(){
    component.reset();
    $('#hooks').html('');
    newHook('created');
    newHook('mounted');
    newHook('updated');
    newHook('destroyed');
    previewController.update();
    updateMethods();
    updateComputed();
    updateWatchers();
    updateList('#slots',component.slots);
    updateList('#events',component.events);
    $('#saveName').val('');
}

function download(){
    let link = document.createElement('a');
    link.href = 'data:text/plain;charset=utf-8,'+vueExporter.export();
    link.download = 'component.vue';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}



// component library
async function openLibrary(){
    $('#openModal').modal('show');
    let names = await localComponentLibrary.all();
    refreshLibrary(names);
    names = await remoteComponentLibrary.all();
    refreshLibrary(names,true);
}

async function openComponent(name, isRemote=false){
    if(isRemote)
        component.unserialize(await remoteComponentLibrary.get(name));
    else
        component.unserialize(await localComponentLibrary.get(name));
    if($('#openForked').prop('checked')){
        component.parent=component.name;
        component.name='forked';
    }
    previewController.update();
    $('#openModal').modal('hide');
    updateMethods();
    updateComputed();
    updateWatchers();
    updateList('#slots',component.slots);
    updateList('#events',component.events);
    $('#saveName').val(component.name);
}

function saveComponent(){
    $('#saveModal').modal('hide');
    component.name=$('#saveName').val();
    if($('#saveLocally').prop('checked'))
        localComponentLibrary.set(component.serialize());
    if($('#saveRemotely').prop('checked'))
        remoteComponentLibrary.set(component.serialize());
}

async function deleteComponent(name, isRemote=false){
    if(isRemote){
        await remoteComponentLibrary.delete(name);
        let names=await remoteComponentLibrary.all();
        refreshLibrary(names,true);
    }
    else{
        await localComponentLibrary.delete(name);
        let names=await localComponentLibrary.all();
        refreshLibrary(names);
    }
}

function refreshLibrary(names, isRemote=false){
    if(isRemote){
        let html='';
        for(let n of names){
            html+=`<a href="javascript:void(0)" class="list-group-item list-group-item-action text-primary" onclick="openComponent('${n}',true)">${n}
        <button type="button" class="btn btn-link" onclick="deleteComponent('${n}',true); event.cancelBubble=true"><i class="fa fa-minus text-danger"></i></button></a>`;
        }
        $('#remoteList').html(html);
    }
    else{
        let html='';
        for(let n of names){
            html+=`<a href="javascript:void(0)" class="list-group-item list-group-item-action text-primary" onclick="openComponent('${n}')">${n}
        <button type="button" class="btn btn-link" onclick="deleteComponent('${n}'); event.cancelBubble=true"><i class="fa fa-minus text-danger"></i></button></a>`;
        }
        $('#localList').html(html);
    }

}
