onload=function(){
    editors= {
        slots: initCodeMirror('#slots', 'htmlmixed'),
        style: initCodeMirror('#style', 'css'),
        events: initCodeMirror('#events', 'javascript'),
        code: initCodeMirror('#code', 'javascript'),
        include: initCodeMirror('#include', 'properties')
    }
    localComponentLibrary=new LocalComponentLibrary();
    remoteComponentLibrary=new RemoteComponentLibrary();
    remoteAppLibrary=new RemoteAppLibrary();
    component=new Component();  //global VM
    app=new App(component);
    cssExporter=new CSSExporter();
    $('head').append(`<style>${cssExporter.export()}</style>`);
    appExporter = new AppExporter(app, cssExporter)
    appInstaller=new AppInstaller(appExporter);
    buildController=new PreviewController('#desktopPreview,#mobilePreview',appExporter);
    manifest={description:'',background_color:'#ffffff',theme_color:'#ffffff'};
    isDesktopLayout=true;
    newLayer('default');
    newHook('created');
    newHook('mounted');
    newHook('updated');
    newHook('destroyed');
    $('#includeModal').on('hidden.bs.modal', function(){closeEditor('include')});
    $('#configurationModal').on('hidden.bs.modal', function(){configure(false)});
    $('a[href="#desktopTab"]').on('shown.bs.tab',function(){isDesktopLayout=true; updateLayout()});
    $('a[href="#mobileTab"]').on('shown.bs.tab',function(){isDesktopLayout=false; updateLayout()});
    configure(false);
}



// layout preview actions
function newLayer(){
    let name=genName();
    let layer=new Layer(name);
    app.set(name,layer);
    selectLayer(name);
}
function selectLayer(name){
    layerName=name;
    selectNode(name);
}
function newRow(){
    let name=genName();
    node.set(name,new Row(name));
    selectNode(name);
}
function selectNode(name){
    openSelection(name);
    updateLayout();
}
function deleteNode(){
    let parent=app.findParent(node.name);
    parent.delete(node.name);
    if(node instanceof Layer){
        let lastTabName=null;
        for(k of app.keys())
            lastTabName=k;
        selectLayer(lastTabName);
    }
    else
        selectNode(parent.name);
}
function swapNodes(inv=false){
    if(node instanceof Layer)
        inv=!inv;
    let parent=app.findParent(node.name);
    let items=[...parent.values()];
    let i=items.indexOf(node);
    let j=(i==0)?items.length-1:i-1;
    if(inv)
        j=(i==items.length-1)?0:i+1;
    let tmp=items[i]; items[i]=items[j]; items[j]=tmp;
    parent.clear();
    for(let item of items)
        parent.set(item.name,item);
    updateLayout()
}



//open node
function openSelection(nodeName) {
    if(nodeName) {
        node = app.findParent(nodeName);
        if (!node) {
            $('#cActions,#cStyle,#cDesktop,#cMobile,#cProperties,#cEvents,#cSlots,#cMethods,#cComputed').hide();
            node = app;
            return;
        }
        node = node.get(nodeName);
        if (node instanceof Layer) {
            $('#aComponent').hide();
            $('#aRow, #aDelete, #aUp, #aDown').show();
            $('#cStyle,#cEvents,#cSlots,#cMethods,#cComputed, #cMobile').hide();
            $('#desktopWidth,#desktopOffset,#desktopAlign,#mobileWidth,#mobileOffset,#mobileAlign').parent().hide();
            $('#desktopPosition').parent().show();
            $('#mobilePosition').parent().show();
            $('#cActions,#cDesktop,#cMobile,#cProperties').show();
            $('#desktopPL,#mobilePL').parent().hide();
            $('#desktopPosition').val(node.desktop.position);
            $('#mobilePosition').val(node.mobile.position);
        }
        else {
            if (node instanceof Row) {
                $('#aRow, #aDelete, #aComponent, #aUp, #aDown').show();
                $('#cStyle,#cEvents,#cSlots,#cMethods,#cComputed').hide();
                $('#cActions,#cDesktop,#cMobile,#cProperties').show();
            }
            else if (node instanceof Col) {
                $('#aRow, #aComponent').hide();
                $('#aDelete, #aUp, #aDown').show();
                $('#cActions,#cStyle,#cDesktop,#cMobile,#cProperties,#cEvents,#cSlots,#cMethods,#cComputed').show();
                openSelectionComponent();
            }
            $('#desktopPosition').parent().hide();
            $('#mobilePosition').parent().hide();
            $('#desktopWidth,#desktopOffset,#desktopAlign').parent().show();
            $('#mobileWidth,#mobileOffset,#mobileAlign').parent().show();
            $('#desktopPL, #mobilePL').parent().show();
            $('#desktopWidth').val(node.desktop.width); $('#desktopWidthL').html(node.desktop.width);
            $('#mobileWidth').val(node.mobile.width); $('#mobileWidthL').html(node.mobile.width);
            $('#desktopOffset').val(node.desktop.offset); $('#desktopOffsetL').html(node.desktop.offset);
            $('#mobileOffset').val(node.mobile.offset); $('#mobileOffsetL').html(node.mobile.offset);
            $('#desktopAlign').val(node.desktop.align);
            $('#mobileAlign').val(node.mobile.align);
            $('#desktopPL').val(node.desktop.pl); $('#desktopPLL').html(node.desktop.pl);
            $('#desktopPT').val(node.desktop.pt); $('#desktopPTL').html(node.desktop.pt);
            $('#desktopPR').val(node.desktop.pr); $('#desktopPRL').html(node.desktop.pr);
            $('#desktopPB').val(node.desktop.pb); $('#desktopPBL').html(node.desktop.pb);
            $('#mobilePL').val(node.mobile.pl); $('#mobilePLL').html(node.mobile.pl);
            $('#mobilePT').val(node.mobile.pt); $('#mobilePTL').html(node.mobile.pt);
            $('#mobilePR').val(node.mobile.pr); $('#mobilePRL').html(node.mobile.pr);
            $('#mobilePB').val(node.mobile.pb); $('#mobilePBL').html(node.mobile.pb);
        }
    }
    let html='';
    let options='';
    for(let k of app.vm.watchers.keys())
        options+=`<option value="${k}">${k}</option>`
    for(let p of node.properties.keys()){
        let name=node.propTypes.get(p)+' '+p+' ';
        html+=`<a href="javascript:void(0)" class="list-group-item passive-list-group-item">${name}<select name="${p}" onchange="updateSelection()">${options}</select></a>`;
        $(`#properties select[name="${p}"]`).val(node.properties.get(p));
    }
    $('#properties').html(html);
    for(let p of node.properties.keys())
        $(`#properties select[name="${p}"]`).val(node.properties.get(p));
}
// select something from dropdowns or sliders
function updateSelection(isLayer){         //uses global node variable to identify currently selected
    if (isLayer) {
        node.desktop.position=$('#desktopPosition').val();
        node.mobile.position=$('#mobilePosition').val();
    }
    else {
        node.desktop.width=$('#desktopWidth').val(); node.mobile.width=$('#mobileWidth').val();
        node.desktop.offset=$('#desktopOffset').val(); node.mobile.offset=$('#mobileOffset').val();
        node.desktop.align=$('#desktopAlign').val(); node.mobile.align=$('#mobileAlign').val();
        node.desktop.pl=$('#desktopPL').val(); node.desktop.pt=$('#desktopPT').val(); node.desktop.pr=$('#desktopPR').val(); node.desktop.pb=$('#desktopPB').val();
        node.mobile.pl=$('#mobilePL').val(); node.mobile.pt=$('#mobilePT').val(); node.mobile.pr=$('#mobilePR').val(); node.mobile.pb=$('#mobilePB').val();
    }
    for(let p of node.properties.keys())
        node.properties.set(p,$(`#properties select[name="${p}"]`).val());
    updateLayout()
}
// additional for component
function openSelectionComponent(){
    if(!node.component) return;
    updateList('#cMethods .list-group',[...node.component.methods.values()].map(e=>e.substring(0,e.indexOf('{'))));
    updateList('#cComputed .list-group',node.component.computed.keys());
    let html='';
    for(let k of node.slots.keys())
        html+=`<a href="javascript:void(0)" class="list-group-item list-group-item-action text-primary" onclick="openComponentEditor('#slotsModal','slots','${k}'); event.cancelBubble=true">${k}</a>`
    $('#cSlots .list-group').html(html);
    html='';
    for(let k of node.events.keys())
        html+=`<a href="javascript:void(0)" class="list-group-item list-group-item-action text-primary" onclick="openComponentEditor('#eventsModal','events','${k}'); event.cancelBubble=true">${k}</a>`
    $('#cEvents .list-group').html(html);
}
//one modal for component's events, slots and style
function openComponentEditor(modalSelector,type,name){
    let modal = $(modalSelector);
    let value=name?node[type].get(name):node[type];
    editors[type].getDoc().setValue(value);
    modal.off('hidden.bs.modal');
    modal.on('hidden.bs.modal', function(){
        let value=editors[type].getDoc().getValue();
        if(name)
            node[type].set(name,value);
        else
            node[type]=value;
    });
    modal.modal('show');
    editors[type].focus();
    editors[type].setCursor(1,1);
    editors[type].refresh();
}


//refreshes layout preview
function updateLayout(){
    let tabs='';
    for (let l of app.values()) {
        tabs+=`<li class="nav-item">
            <a class="nav-link ${l.name==layerName?'active':''}" data-toggle="tab" href="#${l.name}" onclick="selectLayer('${l.name}')" role="tab"><i class="fa fa-window-maximize"></i></a></li>`;
    }
    let content='';
    for (let l of app.values()){
        let pos=isDesktopLayout?l.desktop.position:l.mobile.position;
        let rows='<div class="container-fluid p0">'+this.childrenHTML(l,node.name,true)+'</div>';
        let layerTop=`<div class="tab-pane editor-layer ${l.name==layerName?'active':''} ${(!node.name||l.name==node.name)?'editor-selected':''}" id="${l.name}"
            role="tabpanel" onclick="selectLayer('${l.name}')">`
        let layerBottom=`<div class="editor-transparent">${pos}</div>`;
        let layerBottom2='<div class="editor-transparent"></div>';
        if(pos=='fixed-bottom')
            content+=(layerTop+layerBottom+layerBottom2+rows+'</div>');
        else if(pos=='fixed-center')
            content+=(layerTop+layerBottom+rows+layerBottom2+'</div>');
        else
            content+=(layerTop+rows+layerBottom+layerBottom2+'</div>');
    }
    let html=`<ul class="nav nav-tabs" role="tablist">${tabs}
        <button type="button" class="btn btn-link text-success" title="new layer" onclick="newLayer()"><i class="fa fa-plus fa-large"></i></button></ul>
        <div class="tab-content">${content}</div>`;
    $('#layoutPreview').html(html);
}
function childrenHTML(parentNode,selectedName,l1=false) {         //prints children of node
    let html = '';
    for (let n of parentNode.values()) {
        let cl = 'col-' + (isDesktopLayout?n.desktop.width:n.mobile.width);
        cl += ' offset-' + (isDesktopLayout?n.desktop.offset:n.mobile.offset);
        cl += ' align-self-' + (isDesktopLayout?n.desktop.align:n.mobile.align);
        cl += ' pl'+(isDesktopLayout?n.desktop.pl:n.mobile.pl)+' pt'+(isDesktopLayout?n.desktop.pt:n.mobile.pt)+
        ' pr'+(isDesktopLayout?n.desktop.pr:n.mobile.pr)+' pb'+(isDesktopLayout?n.desktop.pb:n.mobile.pb);
        if(l1)
            html += `<div class="row m0"><div class="editor-row ${cl} ${n.name == selectedName ? 'editor-selected' : ''}" onclick="selectNode('${n.name}');event.cancelBubble=true"><div class="row m0">${childrenHTML(n, selectedName)}</div></div></div>`
        else if (n instanceof Map)
            html += `<div class="editor-row ${cl} ${n.name == selectedName ? 'editor-selected' : ''}" onclick="selectNode('${n.name}');event.cancelBubble=true"><div class="row m0">${childrenHTML(n, selectedName)}</div></div>`
        else
            html += `<div class="editor-col ${cl} ${n.name == selectedName ? 'editor-selected' : ''}" onclick="selectNode('${n.name}');event.cancelBubble=true"><div class="editor-comp">${n.name}</div></div>`;
    }
    return html;
}

function genName(){
    if(typeof _lastName !== 'undefined')
        _lastName++;
    else
        _lastName=1;
    return 'n'+Math.ceil(Math.random()*1000)+_lastName;
}





//component library
async function openComponentSelection(){
    $('#componentModal').modal('show');
    let names = await localComponentLibrary.all();
    let html='';
    for(let n of names)
        html+=`<a href="javascript:void(0)" class="list-group-item list-group-item-action text-primary" onclick="openComponent('${n}')">${n}</a>`;
    $('#localList').html(html);
    names = await remoteComponentLibrary.all();
    html='';
    for(let n of names)
        html+=`<a href="javascript:void(0)" class="list-group-item list-group-item-action text-primary" onclick="openComponent('${n}',true)">${n}</a>`;
    $('#remoteList').html(html);
}
async function openComponent(componentName, isRemote){
    $('#componentModal').modal('hide');
    openName('#nameModal',async function(name){
        if(!name||!name[0].match(/^[a-z_\$]/i))
            name=genName();
        let component=new Component();
        if(isRemote)
            component.unserialize(await remoteComponentLibrary.get(componentName));
        else
            component.unserialize(await localComponentLibrary.get(componentName));
        node.set(name,new Col(name,component));
        selectNode(name);
    });
}


//update preview
function updatePreview(){
    $('#previewModal').modal('show');
    buildController.update();
}

// open/save app
async function openApps(){
    $('#appModal').modal('show');
    names = await remoteAppLibrary.all();
    refreshApps(names);
}
async function openApp(name){
    app.unserialize(await remoteAppLibrary.get(name));
    $('#appModal').modal('hide');
    let layers=[...app.keys()];
    if(layers.length>0)
        selectLayer(layers[0]);
    else
        selectNode(app.name);
    updateMethods();
    updateComputed();
    updateWatchers();
    $('#saveName').val(app.name);
    $('#appName').val(app.name);
}
function saveApp(){
    $('#saveModal').modal('hide');
    app.name=$('#saveName').val();
    remoteAppLibrary.set(app.serialize());
}
async function deleteApp(name){
    await remoteAppLibrary.delete(name);
    let names=await remoteAppLibrary.all();
    refreshApps(names,true);
}
function refreshApps(names){
    let html='';
    for(let n of names) {
        html += `<a href="javascript:void(0)" class="list-group-item list-group-item-action text-primary" onclick="openApp('${n}')">${n}
        <button type="button" class="btn btn-link" onclick="deleteApp('${n}',true); event.cancelBubble=true"><i class="fa fa-minus text-danger"></i></button></a>`;
    }
    $('#appList').html(html);

}

function install(){
    appInstaller.install(app);
}

function configure(configure=true){
    if(configure){
        remoteAppLibrary.apiURL=$('#configurationModal #aApiURL').val();
        remoteComponentLibrary.apiURL=$('#configurationModal #cApiURL').val();
        appInstaller.apiURL=$('#configurationModal #installApiURL').val();
        appInstaller.vueURL=appExporter.vueURL=$('#configurationModal #vueURL').val();
        appInstaller.bsGridURL=appExporter.bsGridURL=$('#configurationModal #bsGridURL').val();
        app.name=$('#configurationModal #appName').val();
        manifest.description=$('#configurationModal #appDescription').val();
        manifest.background_color=$('#configurationModal #appBackground').val();
        manifest.theme_color=$('#configurationModal #appTheme').val();
        appExporter.background=manifest.background_color;
        $('#saveName').val(app.name);
        buildController.update();
        $('#configurationModal').modal('hide');

    }
    else{
        $('#configurationModal #aApiURL').val(remoteAppLibrary.apiURL);
        $('#configurationModal #cApiURL').val(remoteComponentLibrary.apiURL);
        $('#configurationModal #installApiURL').val(appInstaller.apiURL);
        $('#configurationModal #vueURL').val(appExporter.vueURL);
        $('#configurationModal #bsGridURL').val(appExporter.bsGridURL);
        $('#configurationModal #appName').val(app.name);
        $('#configurationModal #appDescription').val(manifest.description);
        $('#configurationModal #appBackground').val(manifest.background_color);
        $('#configurationModal #appTheme').val(manifest.theme_color);
		$('#saveName').val(app.name);
    }
}