class PreviewExporter{
    constructor(component){
        this.component = component;
        this.vueURL='https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.13/vue.min.js';
        this.bsGridURL='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap-grid.min.css';
        this.bgURL='https://i.pinimg.com/236x/b3/4b/d9/b34bd9c48f86e10225785c54e4953300.jpg';
        this.width=6;
        this.border='2px dashed red';
    }
    export(){
        return `data:text/html;charset=utf-8,
<!--${Math.random()}-->
<html>
<head>
<script>
function showError(at, err, line) {
  parent.postMessage({version:Vue.version,at,err,line},"*");
}
window.onerror=function(msg, url, ln) {
  showError('parsing',msg,ln);
  return true;
}
</script>
${this.component.head}
<script src="${this.vueURL}"></script>
<link rel="stylesheet" href="${this.bsGridURL}">
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
${this.component.style}
</style>
</head>
<body style="background-image:url(${this.bgURL}); margin:0px">
<div class="container-fluid" style="min-height:100%;padding:0">
<div class="row justify-content-center align-items-center" style="min-height:100%; margin:0" id="app">
<component ></component>
</div>
</div>
<script>
Vue.component('component', {
data: function(){return {
${this.component.data}
}},
props:{
${this.component.props}
},
${this.component.hooks},
template:\`<div class="col-${this.width}" style="border:${this.border}; padding:0">
${this.component.template}
</div>\`,
methods:{
${this.component.methods}
},
computed:{
${this.component.computed}
},
watch:{
${this.component.watchers}
}
});
Vue.config.errorHandler=function (err, vm, info) {
    let ln=err.stack.toString().split('\\n')[1];
    ln=ln.substring(0,ln.lastIndexOf(':'));
    ln=ln.substring(ln.lastIndexOf(':')+1);
    showError(info, err.toString(),ln);
};
new Vue({el: '#app'});
</script>
</body>
</html>`
    }
}


class VueExporter{
    constructor(component){
        this.component = component;
    }
    export(){
        return `<template>
    <div>
\t\t${this.component.template.replace(/\n/g,'\n\t\t')}
    </div>
</template>

<style>
\t${this.component.style.replace(/\n/g,'\n\t')}
</style>

<script>
export default{
    data: function(){return {
\t\t${this.component.data.toString().replace(/\n/g,'\n\t\t')}
    }},
    props:{
\t\t${this.component.props.replace(/\n/g,'\n\t\t')}
    },
\t${this.component.hooks.toString().replace(/\n/g,'\n\t')},
    methods:{
\t\t${this.component.methods.toString().replace(/\n/g,'\n\t\t')}
    },
    computed:{
\t\t${this.component.computed.toString().replace(/\n/g,'\n\t\t')}
    },
    watch:{
\t\t${this.component.watchers.toString().replace(/\n/g,'\n\t\t')}
    }
}
</script>
`
    }
}



class AppExporter{
    constructor(app, cssExporter){
        this.app = app;
        this.vueURL='https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.13/vue.min.js';
        this.bsGridURL='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap-grid.min.css';
        this.cssExporter=cssExporter;
        this.background='#ffffff';
    }
    export(sw){
        this.styles=[];
        this.code=[];
        this.methods=[];
        this.heads=[];
        this.names=[];
        let metStr=this.app.vm.methods.toString();
        if(metStr.length>0) this.methods.push(metStr);
        this.html=this.generate();
        return `${sw?``:`data:text/html;charset=utf-8, <!--${Math.random()}-->`}
<html>
<head>
${sw?`<link rel="manifest" href="manifest.json?${Math.random()}">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="application-name" content="${this.app.name}">
    <meta name="apple-mobile-web-app-title" content="${this.app.name}">
    <meta name="msapplication-starturl" content="index.html">
    <link rel="icon" href="icon192.png">
    <link rel="apple-touch-icon" href="icon192.png">
	<meta name="theme-color" content="${this.background}"/>
	<noscript>JavaScript is not enabled!</noscript>
    <title>${this.app.name}</title>`:``}
<script>
${sw?`if ('serviceWorker' in navigator)
    navigator.serviceWorker.register('${sw}');`:``}
function showError(at, err, line) {
  parent.postMessage({version:Vue.version,at,err,line},"*");
}
window.onerror=function(msg, url, ln) {
  showError('parsing',msg,ln);
}
</script>
${this.app.vm.head}
${this.heads.join('\n')}
<script src="${this.vueURL}"></script>
<link rel="stylesheet" href="${this.bsGridURL}">
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
${this.cssExporter.export()}
${this.styles.join('\n')}
</style>
</head>
<body style="background:${this.background}">
${this.html}
<script>
Vue.config.errorHandler=function (err, vm, info) {
    let ln=err.stack.toString().split('\\n')[1];
    ln=ln.substring(0,ln.lastIndexOf(':'));
    ln=ln.substring(ln.lastIndexOf(':')+1);
    showError(info, err.toString(),ln);
};
${this.code.join('\n')};
app=new Vue({el: '#app',
data: function(){return {
${this.app.vm.data}
}},`+
`
${this.app.vm.hooks},
methods:{
${this.methods.join(',\n')}
},
computed:{
${this.app.vm.computed}
},
watch:{
${this.app.vm.watchers}
}`.replace(new RegExp('(this\\.)('+this.names.join('|')+')(?=[\\W]|$)','g'),`this.$refs['$2']`)+`
});
</script>
</body>
</html>`
    }

    generate(){
        let html='<div id="app" class="e0">';
        for (let l of this.app.values()){
            if(l.desktop.position=='static')
                html+=`<div class="container-fluid p0" ${l.properties.get('display')?'v-if="'+l.properties.get('display')+'"':''}>${this._generate(l,true)}</div>`;
            else if(l.desktop.position=='absolute')
                html+=`<div class="container-fluid p0 position-absolute" ${l.properties.get('display')?'v-if="'+l.properties.get('display')+'"':''}>${this._generate(l,true)}</div>`;
            else{
                let al='align-self-start';
                if(l.desktop.position=='fixed-bottom')
                    al='align-self-end'
                if(l.desktop.position=='fixed-center')
                    al='align-self-center'
                html+=`<div class="container-fluid p0 h100 position-fixed" ${l.properties.get('display')?'v-if="'+l.properties.get('display')+'"':''}><div class="row h100 m0"><div class="col-12 p0 ${al}">${this._generate(l,true)}</div></div></div>`;
            }
        }
        return html;
    }
    _generate(parentNode,l1=false) {         //prints children of node
        let html = '';
        for (let n of parentNode.values()) {
            let cl = 'col-sm-' + n.desktop.width;
            cl += ' col-' + n.mobile.width;
            cl += ' offset-sm-' + n.desktop.offset;
            cl += ' offset-' + n.mobile.offset;
            cl += ' align-self-sm-' + n.desktop.align;
            cl += ' align-self-' + n.mobile.align;
            cl += ' pl'+n.mobile.pl+' pt'+n.mobile.pt+' pr'+n.mobile.pr+' pb'+n.mobile.pb;
            cl += ' plsm'+n.desktop.pl+' ptsm'+n.desktop.pt+' prsm'+n.desktop.pr+' pbsm'+n.desktop.pb;
            if(l1)
                html += `<div class="row m0"><div class="${cl}" ${n.properties.get('display')?'v-if="'+n.properties.get('display')+'"':''}><div class="row m0">${this._generate(n)}</div></div></div>`
            else if (n instanceof Map)
                html += `<div class="${cl}" ${n.properties.get('display')?'v-if="'+n.properties.get('display')+'"':''}><div class="row m0">${this._generate(n)}</div></div>`
            else {
                let events=[];
                for(let k of n.events.keys())
                    events.push('@'+k+'="'+n.name+k+'"');
                let properties=[];
                for( let [k,v] of n.properties){
                    if(v){
                        if(k=='display')
                            properties.push(`v-if="${v}"`);
                        else
                            properties.push(`:${k}.sync="${v}"`);
                    }
                }
                html += `<${n.component.name} class="${cl} e1" id="${n.name}" ${events.join(' ')} ${properties.join(' ')} ref="${n.name}">${this.genSlots(n).join('\n')}</${n.component.name}>`;
                this.genCode(n,cl);
                this.genStyle(n);
                this.genHandlers(n);
                this.heads.push(n.component.head);
                this.names.push(n.name);
            }
        }
        return html;
    }
    genStyle(node){
        let style=node.style.replace(/\s+(\.[a-z0-9_$#\.\-]+)/ig,'#'+node.name+' $1');
        this.styles.push(style);
    }
    genCode(node){
        this.code.push(`Vue.component('${node.component.name}', {
            data: function(){return {
            ${node.component.data}
            }},
            props:{
            ${node.component.props}
            },
            ${node.component.hooks},
            template:\`<div class="">${node.component.template}</div>\`,
            methods:{
            ${node.component.methods}
            },
            computed:{
            ${node.component.computed}
            },
            watch:{
            ${node.component.watchers}
            }
            });
            `);
    }
    genHandlers(node){
        let html='';
        for(let [k,v] of node.events)
            html+=node.name+k+':'+v;
        if(html.length>0)
        this.methods.push(html);
    }
    genSlots(node){
        let slots=[];
        for(let [k,v] of node.slots) {
            if(node.slotArgs.get(k).length>0){
                let regexp=new RegExp('(\\W|^)('+node.slotArgs.get(k).join('|')+')(\\W|$)','ig');
                let repl='$1slotProps.$2$3'
                slots.push(`<template slot="${k}" slot-scope="slotProps">${v.replace(regexp,repl)}</template>`);
            }
            else
                slots.push(`<template slot="${k}">${v}</template>`);
        }
        return slots;
    }
}


class CSSExporter{
    constructor(desktopMinWidth=576){
        this.desktopMinWidth=desktopMinWidth;
    }
    export(){
        let style=`
.position-absolute {position: absolute !important;}
.position-fixed {position: fixed !important;}
.m0 {margin-left:0;margin-right:0;}
.p0 {padding-left:0;padding-right:0;}
.h100 {height:100%;}
.e0 {pointer-events:none;}
.e1 {pointer-events:auto;}
body {margin:0px;}`;
            for(let i=0;i<31;i++)
                style+=`.pl${i}{padding-left:${i*2/10}rem}
.pt${i}{padding-top:${i*2/10}rem}
.pr${i}{padding-right:${i*2/10}rem}
.pb${i}{padding-bottom:${i*2/10}rem}\n`;
            style+=`@media (min-width: ${this.desktopMinWidth}px){\n`;
            for(let i=0;i<31;i++)
                style+=`.plsm${i}{padding-left:${i*2/10}rem}
.ptsm${i}{padding-top:${i*2/10}rem}
.prsm${i}{padding-right:${i*2/10}rem}
.pbsm${i}{padding-bottom:${i*2/10}rem}\n`;
        return style+'}';
    }
}

