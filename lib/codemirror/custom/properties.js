// CodeMirror, copyright (c) by Marijn Haverbeke and others
// Distributed under an MIT license: http://codemirror.net/LICENSE

(function(mod) {
  if (typeof exports == "object" && typeof module == "object") // CommonJS
    mod(require("../../lib/codemirror"), require("../../addon/mode/simple"));
  else if (typeof define == "function" && define.amd) // AMD
    define(["../../lib/codemirror", "../../addon/mode/simple"], mod);
  else // Plain browser env
    mod(CodeMirror);
})(function(CodeMirror) {
  "use strict";

  // Collect all Dockerfile directives
  var types = ["string", "number", "boolean", "function", "object", "array", "symbol"],
      type = new RegExp("(" + types.join('|') + ")" + "(\\s+)", "i"),
      name = new RegExp("[a-z0-9_$]+(?=[ =]*)", "i"),
      nameOnly = new RegExp("[a-z0-9_$]+(?=[ =]*$)", "i"),
      rest = new RegExp("[^#]*", "i");

  CodeMirror.defineSimpleMode("properties", {
    start: [
      // Block comment: This is a line starting with a comment
      {
        regex: /#.*$/,
        token: "comment"
      },
      // Highlight an instruction without any arguments (for convenience)
      {
        regex: type,
        token: "quote"
      },
      // name
      {
        regex: nameOnly,
        token: "def",
        next: "start"
      },
      {
        regex: name,
        token: "def",
        next: "end"
      },
    ],
      end: [
        {
          regex: rest,
          token: "negative",
          next: "start"
        },
      ],
      meta: {
          lineComment: "#"
      }
  });

});
