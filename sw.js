VERSION='v4';

this.addEventListener('install', function(event) {
  event.waitUntil(
    caches.open(VERSION).then(function(cache) {
      return cache.addAll([	
        'icon192.png',
		'builder.html',
		'index.html',
		'component.html',
		'static/style.css',
		'manifest.json',
		'static/chessboard.png'
      ]);
    })
  );
});

this.addEventListener('activate', function(event) {
  event.waitUntil(caches.keys().then(function(keyList) {
    return Promise.all(keyList.map(function(key) {
      if (key!=VERSION) {
        return caches.delete(key);
      }
    }));
  }));
});

this.addEventListener('fetch', function(event) {
  event.respondWith(caches.match(event.request).then(
    function(response) {
      return response || fetch(event.request).then(function(response) {
        return caches.open(VERSION).then(function(cache) {
          cache.put(event.request, response.clone());
          return response;
        });  
      });
    })
  );
});