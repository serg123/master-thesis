<?php
header("Access-Control-Allow-Origin: *");
$json=json_decode(file_get_contents('php://input'));
if(is_dir($json->name)){
	@unlink($json->name."/index.html");
	@unlink($json->name."/sw.js");
	@unlink($json->name."/manifest.json");
	@unlink($json->name."/icon192.png");
	@rmdir($json->name);
}
mkdir($json->name);
$file = fopen($json->name."/index.html", "w");
fwrite($file, $json->app);
fclose($file);
$file = fopen($json->name."/sw.js", "w");
writeSW($file, $json->cache);
fclose($file);
$file = fopen($json->name."/manifest.json", "w");
writeManifest($file, $json->manifest);
fclose($file);
copy("icon192.png",$json->name."/icon192.png");

function writeSW($file, $cache){
	fwrite($file,"VERSION='v".rand()."';
this.addEventListener('install', function(event) {
  event.waitUntil(
    caches.open(VERSION).then(function(cache) {
      return cache.addAll(['manifest.json','icon192.png',
");
	foreach($cache as $c)
		fwrite($file,"'".$c."',\n");
	fwrite($file,"]);
    })
  );
});
this.addEventListener('activate', function(event) {
  event.waitUntil(caches.keys().then(function(keyList) {
    return Promise.all(keyList.map(function(key) {
      if (key!=VERSION) {
        return caches.delete(key);
      }
    }));
  }));
});
this.addEventListener('fetch', function(event) {
  event.respondWith(caches.match(event.request).then(
    function(response) {
      return response || fetch(event.request);
    })
  );
});");
}

function writeManifest($file, $manifest){
	$fields="";
	foreach ($manifest as $key => $value)
		$fields=$fields."\"".$key."\": \"".$value."\",\n";
	fwrite($file,"{".$fields.	
"\"start_url\": \"index.html\",
\"display\": \"standalone\",
\"orientation\": \"any\",
\"icons\": [{
	\"src\": \"icon192.png\",
	\"sizes\": \"192x192\",
	\"type\": \"image/png\"
  }]
}");
}
?>