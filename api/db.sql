create database ComponentLibrary;
use ComponentLibrary;
create table ComponentStore(
	name varchar(100) primary key,
	component text
);