## open index.html in browser

## folder structure (files that are not listed here were used during development but are not important for the final result):
- api - server-side functionality
- js - client-side scripts
- lib - used 3-rd party assets (there only codemirror/custom/properties.js is added)
- static - used non-code assets (css styles are here)
- index.html - contains description and links
- component.html - component creator
- builder.html - application builder